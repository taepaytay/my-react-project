import React, { Component } from 'react';
import './App.css';
import Person from '../Person/Person.js';
import Clock from '../Clock/Clock';

class App extends Component {
  stateStatus = 1;
  state ={
    person: [
      {name: 'Tae', age: 22},
      {name: 'NOON', age: 22},
      {name: 'abc', age: 9}
    ],
    otherState: 'some other value'
  }

  switchNameHandler = () => {
    // console.log('Was Clicked!');
    // this.state.person[0].name = 'Taecha';
    console.log(this.stateStatus);
    if(this.stateStatus){
      this.stateStatus = 0;      
      this.setState({
        person: [
          {name: 'Taecha', age: 50},
          {name: 'NOON', age: 22},
          {name: 'abc', age: 9}
        ]
      })
    }else{
      this.stateStatus = 1;
      this.setState({
        person: [
          {name: 'Tae', age: 22},
          {name: 'NOON', age: 22},
          {name: 'abc', age: 9}
        ]
      })
    }
  }

  render() {
    return (
      <div className="App">
        <h1>Hi this react</h1>
        <button onClick={this.switchNameHandler}>Switch Name</button>
        <Clock/>
        {/* <input></input> */}
        <Person name = {this.state.person[0].name} age = {this.state.person[0].age}/>
        <Person name = {this.state.person[1].name} age = {this.state.person[1].age}> HIHII</Person>
        <Person name = {this.state.person[2].name} age = {this.state.person[2].age}/>
        <form/>
      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'ITS WORK!'));
  }
}

export default App;
