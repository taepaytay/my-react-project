import React from 'react';
import './Person.css'

const person = (props) => {
    return (
        <div className="person">
            <p>I'm {props.name} !, I am {props.age} yesrs old </p>
            <p>{props.children}</p>
        </div>
    )
}

export default person;
